'use srtrict'

let clientName = prompt("Enter your name");

if (clientName === "" || clientName === null || !isNaN(clientName)) {
  alert("You are not allowed to visit this website, you should enter your name");
}
  else {
    let clientAge = +prompt("Enter your age");
    if (isNaN(clientAge) || clientAge === null || clientAge === "") {
      clientAge = prompt("Please, enter your age again");
      if (isNaN(clientAge) || clientAge === "" || clientAge === null) {
        alert("You are not allowed to visit this website");
      }
    }
    if (clientAge < 18) {
      alert("You are not allowed to visit this website");
    } else if (18 <= clientAge && clientAge <= 22) {
      let clientAnswer = confirm("Are you sure you want to continue?");
      if (clientAnswer) {
        alert(`Welcome ${clientName}`);
      } else {
        alert("You are not allowed to visit this website");
      }
    } else if (clientAge > 22) {
      alert(`Welcome ${clientName}`);
    }
  }
;